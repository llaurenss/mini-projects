def move(f, t):
    print("Move disc from: {} to {}".format(f, t))

def moveBy(f, b, t):
    move(f, b)
    move(b, t)

def hanoi(n, f, b, t):
    if n == 0: 
        pass
    else:
        hanoi(n-1, f, t, b)
        move(f, t)
        hanoi(n-1, b, f, t)

hanoi(3, "A", "B", "C")