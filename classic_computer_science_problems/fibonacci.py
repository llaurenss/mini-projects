#fibinacci sequence 
# Leonardo van Pisa 

# recursive 
def fib(n):
    if(n == 0): 
        return 0
    elif (n == 1):
        return 1
    else:
        return (fib(n-1) + fib(n-2))

def fib_dynamic(n):
    seq = [0, 1]
    i = 2
    while i < n:
        a = seq[i - 1]
        b = seq[i - 2]
        s = a + b
        seq.append(s)
        i += 1
    size = len(seq)
    result = seq[size-1] + seq[size-2]
    return result

print("The fib sequence fib(4) -> " + str(fib(4)))
print("The fib sequence fib(5) -> " + str(fib(5)))
print("The fib sequence fib(6) -> " + str(fib(6)))
print("The fib sequence fib(7) -> " + str(fib(7)))
print("The fib sequence fib(8) -> " + str(fib(8)))
print("The fib sequence fib(9) -> " + str(fib(9)))

print("The fib sequence fib(4) -> " + str(fib_dynamic(4)))
print("The fib sequence fib(5) -> " + str(fib_dynamic(5)))
print("The fib sequence fib(6) -> " + str(fib_dynamic(6)))
print("The fib sequence fib(7) -> " + str(fib_dynamic(7)))
print("The fib sequence fib(8) -> " + str(fib_dynamic(8)))
print("The fib sequence fib(9) -> " + str(fib_dynamic(9)))