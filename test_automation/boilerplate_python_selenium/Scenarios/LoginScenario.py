from ..PageObjects.LoginPage import LoginPage
from ..PageObjects.HeaderBox import HeaderBox

class LoginScenario:

    @staticmethod
    def login(driver, username, password):
        loginPage = LoginPage(driver)
        headerBox = HeaderBox(driver)

        loginPage.fill_login_form(username, password)
        loginPage.click_login()
        logged_user = headerBox.get_logged_in_user()
