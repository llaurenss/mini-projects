import time

from selenium import webdriver

from .SeleniumOperations import SeleniumOperations
from .BrowserOptions import BrowserOptions

class SeleniumDriver:

    def __init__(self, browser, url=None):
        self.browserOptions = BrowserOptions()
        self.driver = self.__get_driver_browser(browser)
        self.__set_driver_implicit_waiting()
        self.operations = SeleniumOperations(self)

        if url is not None:
            self.driver.get(url)

    def __set_driver_implicit_waiting(self, timeout=10):
        self.driver.implicitly_wait(timeout)

    def __get_driver_browser(self, browser):
        if browser == 'FIREFOX':
            return webdriver.Firefox()
        else:
            return webdriver.Chrome(options=self.browserOptions.get_options())

    def get_driver(self):
        return self.driver

    def close_driver(self):
        if self.driver is None:
            return
        self.driver.close()

    def go_to(self, url):
        self.driver.get(url)

    def get_title(self):
        return self.driver.title

    # default sleeping time is 2 seconds
    def sleep(self, timeout=2):
        time.sleep(timeout)
