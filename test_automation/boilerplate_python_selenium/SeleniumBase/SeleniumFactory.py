from selenium import webdriver
from .SeleniumDriver import SeleniumDriver

class SeleniumFactory:
    def __init__(self):
        pass

    def get_driver(self, browser, url=None):
        return SeleniumDriver(browser, url)
