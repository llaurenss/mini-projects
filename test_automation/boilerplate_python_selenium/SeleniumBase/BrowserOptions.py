from selenium.webdriver.chrome.options import Options

class BrowserOptions:

    def __init__(self):
        pass

    def get_options(self):
        options = Options()
        options.add_argument("--start-maximized")
        return options
