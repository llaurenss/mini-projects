from selenium.webdriver.common.by import By

class SeleniumOperations:

    def __init__(self, seleniumDriver):
        self.driver = seleniumDriver.get_driver()

    def get_element(self, locatorType, locator):
        if locatorType == "xpath":
            return self.driver.find_element(By.XPATH, locator)
        elif locatorType == "name":
            return self.driver.find_element(By.NAME, locator)
        else:
            return self.driver.find_element(By.ID, locator)

    def enter_text(self, element, text, clear=True):
        if clear:
            element.clear()
        element.send_keys(text)

    def click(self, element):
        element.click()

    def get_text(self, element):
        return element.text
