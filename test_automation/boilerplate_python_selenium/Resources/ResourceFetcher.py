import json
import os

class ResourceFetcher:

    def __init__(self):
        self.__load_config()

    def __load_config(self):
        ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
        with open(ROOT_DIR + "\\config.json", "r") as reader:
            self.config = json.load(reader)

    def get_config(self):
        return self.config
