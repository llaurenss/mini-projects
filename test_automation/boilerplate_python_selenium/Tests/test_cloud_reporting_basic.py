from ..SeleniumBase.SeleniumFactory import SeleniumFactory
from .BaseTest import BaseTest


from ..PageObjects.LoginPage import LoginPage
from ..PageObjects.HeaderBox import HeaderBox

class TestBasic(BaseTest):

    def test_is_reachable(self):
        # assert
        assert self.config["browserTitle"] == self.driver.get_title(), "The title did not match the UI did not load!"

    def test_login_is_working(self):
        # arrange
        loginPage = LoginPage(self.driver)
        headerBox = HeaderBox(self.driver)
        username = self.config["testUser"]["user"]
        password = self.config["testUser"]["pass"]

        # act
        loginPage.fill_login_form(username, password)
        loginPage.click_login()
        self.driver.sleep(10)
        logged_user = headerBox.get_logged_in_user()

        # assert
        assert self.config["browserTitle"] == self.driver.get_title(), "The title did not match the UI did not load!"
        assert logged_user == username, "The logged user did not match what we expected!"
