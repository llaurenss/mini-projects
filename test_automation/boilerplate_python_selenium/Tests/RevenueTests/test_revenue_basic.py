from ...SeleniumBase.SeleniumFactory import SeleniumFactory
from ..BaseTest import BaseTest

from ...Scenarios.LoginScenario import LoginScenario

from ...PageObjects.HomePage import HomePage
from ...PageObjects.HeaderBox import HeaderBox

class TestRevenueBasic(BaseTest):

    def test_revenue_basic(self):
        # arrange
        homePage = HomePage(self.driver)
        headerBox = HeaderBox(self.driver)
        username = self.config["testUser"]["user"]
        password = self.config["testUser"]["pass"]

        # act
        LoginScenario.login(self.driver, username, password)
        homePage.click_reporting()
        current_title = headerBox.get_current_page_title()
        self.driver.sleep(30)
        # assert
        assert current_title == "Reporting", "The logged user did not match what we expected!"
