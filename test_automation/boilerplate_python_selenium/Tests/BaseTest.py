from ..SeleniumBase.SeleniumFactory import SeleniumFactory
from ..Resources.ResourceFetcher import ResourceFetcher

class BaseTest:

    def setup_method(self, method):
        self.config = ResourceFetcher().get_config()
        self.driver = SeleniumFactory().get_driver(self.config["browser"])
        self.driver.go_to(self.config["rootUrl"])

    def teardown_method(self, method):
        self.driver.close_driver()
