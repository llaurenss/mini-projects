class HeaderBox:

    def __init__(self, seleniumDriver):
        self.seleniumDriver = seleniumDriver
        self.bar_user_locator = "//*[@class='bar-user']/span[2]"
        self.current_page_title_locator = "//*/ent-header//*[contains(@class, 'comp-title')]/h1"

    def get_logged_in_user(self):
        usernameField = self.seleniumDriver.operations.get_element("xpath", self.bar_user_locator)
        return self.seleniumDriver.operations.get_text(usernameField)

    def get_current_page_title(self):
        title = self.seleniumDriver.operations.get_element("xpath", self.current_page_title_locator)
        return self.seleniumDriver.operations.get_text(title)
