class HomePage:

    def __init__(self, seleniumDriver):
        self.seleniumDriver = seleniumDriver
        self.reporting_button_locator = "//*/home-item//*[text()='Reporting']/.."

    def click_reporting(self):
        reporting_button = self.seleniumDriver.operations.get_element("xpath", self.reporting_button_locator)
        self.seleniumDriver.operations.click(reporting_button)
