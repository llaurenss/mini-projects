class LoginPage:

    def __init__(self, SeleniumDriver):
        self.seleniumDriver = SeleniumDriver
        self.username = "username"
        self.password = "password"
        self.login_button = "//*/user-access//button"

    def fill_login_form(self, username, password):
        usernameField = self.seleniumDriver.operations.get_element("name", self.username)
        passwordField = self.seleniumDriver.operations.get_element("name", self.password)
        self.seleniumDriver.operations.enter_text(usernameField, username)
        self.seleniumDriver.operations.enter_text(passwordField, password)

    def click_login(self):
        login_button = self.seleniumDriver.operations.get_element("xpath", self.login_button)
        self.seleniumDriver.operations.click(login_button)
