package puzzle;

import java.util.Collection;


public class Node implements Comparable<Node>{

	
	private static PriorityFunc heuristic;
	private Board board;
	private int moves;
	private Node prev;
	
	public Node(Board board, Node prev){
		this.board = board;
		this.prev = prev;
		if(prev == null) this.moves = 0;
		else this.moves = prev.moves + 1;
	}
	
	
	@Override
	public int compareTo(Node o) {
		if(this.heuristic == null || this.heuristic.equals(PriorityFunc.MANHATTAN))
			return (this.board.manhattan() - o.board.manhattan()) + (this.moves - o.moves);
		else
			return (this.board.hamming() - o.board.hamming()) + (this.moves - o.moves);
		
	}
	
	@Override
	public String toString(){
		return this.board.toString();
	}
	
	public void setPriorityFunc(PriorityFunc heurisitc){
		this.heuristic = heurisitc;
	}
	
	public boolean isGoal(){
		return this.board.hamming() == 0;
	}
	
	public Collection<Board> getNeighbors(){
		return this.board.neighbors();
	}
	
	public Node getPrevious(){
		return this.prev;
	}
	
	public Board getBoard(){
		return this.board;
	}
	
	public int getMoves(){
		return this.moves;
	}
}