package puzzle;

import java.util.Collection;
import java.util.PriorityQueue;
import java.util.Stack;


public class Solver
{		
	/**
	 * Finds a solution to the initial board.
	 *
	 * @param priority is either PriorityFunc.HAMMING or PriorityFunc.MANHATTAN
	 */
	private Node initialNode;
	private Node lastNode;
	public Solver(Board initial, PriorityFunc priority)
	{	
		PriorityQueue<Node> queue = new PriorityQueue<Node>();
    	
		if (priority == PriorityFunc.HAMMING) {
			initialNode = new Node(initial, null);
			initialNode.setPriorityFunc(PriorityFunc.HAMMING);
		} else if (priority == PriorityFunc.MANHATTAN) {
			initialNode = new Node(initial, null);
			initialNode.setPriorityFunc(PriorityFunc.MANHATTAN);
		} else {
			throw new IllegalArgumentException("Priority function not supported");
		}
		
		queue.add(initialNode);
		boolean goal = false;
		while(!goal){
			lastNode = generateNeighbors(queue);
			goal= lastNode != null;
		}
	
	}
	
	
	public Node getLastNode(){
		return this.lastNode;
	}
	
	
	
	private Node generateNeighbors(PriorityQueue<Node> q) {
		if(q.isEmpty()) return null;
		Node current = q.remove();
		if(current.isGoal()) return current;
		
		for(Board neighbor : current.getNeighbors()){
			if(current.getPrevious() == null || !neighbor.equals(current.getPrevious().getBoard()))
				q.add(new Node(neighbor, current));
		}
		return null;
	}
	
	public int moves(){
		if(lastNode!= null)
			return lastNode.getMoves();
		return 0;
	}
	
	/**
	 * Returns a Collection of board positions as the solution. It should contain the initial
	 * Board as well as the solution (if these are equal only one Board is returned).
	 */
	public Collection<Board> solution()
	{
		if(lastNode != null){
			Stack<Board> res = new Stack<Board>();
			for(Node tail = lastNode; tail!= null; tail = tail.getPrevious())
				res.push(tail.getBoard());
			return res;
		}
		return null;
	}	
	
}


