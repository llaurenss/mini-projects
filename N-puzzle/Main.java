package puzzle;


class Main
{
	public static void main( String[] args )
	{

		int tiles[][] = {{0,1,3},{4,2,5},{7,8,6}};	
		//int tiles[][] = { {0, 15, 14, 13}, {12, 11, 10, 9},{8, 7, 6, 5}, {4, 3, 2, 1}};  impos
		Board initial = new Board(tiles);
		if (!initial.isSolvable())
		{
			System.out.println("No solution possible");
		}
		else
		{
			Solver solver = new Solver(initial, PriorityFunc.HAMMING);
			for (Board board : solver.solution())
				System.out.println(board);

			//System.out.println("Minimum number of moves = " + solver.solution().size());
			System.out.println("Minimum number of moves = " + solver.moves());
		}
	}
}


