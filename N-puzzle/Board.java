package puzzle;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class Board
{
	private int[][] tiles;
	
	
	
	public Board( int[][] tiles )
	{
		this.tiles = tiles;
	}
	
	// return number of blocks out of place
	public int hamming()
	{
		int[] flatArray = Arrays.stream(tiles).flatMapToInt(Arrays::stream).toArray();
		int heuristic = 0;
		
		for(int i = 0; i < flatArray.length; i++)
			if(i+1 != flatArray[i] && flatArray[i] != 0)
				heuristic++;
		return heuristic;
	}
	
	// return sum of Manhattan distances between blocks and goal
	public int manhattan()
	{
		int heuristic = 0;
    	for (int i = 0; i < this.tiles.length; i++) {
    		for (int j = 0; j < this.tiles.length; j++) {
    			if (this.tiles[i][j] != 0) {
	    			int row = (this.tiles[i][j] - 1 ) / this.tiles.length;
	    			int col = (this.tiles[i][j] - 1) - (row * this.tiles.length);
	    			heuristic += Math.abs(row - i) + Math.abs(col - j);
    			}
    		}
    	}
    	return heuristic;
	}
	
	// does this board position equal y
	public boolean equals(Object y)
	{
		if(y != null && this.getClass().equals(y.getClass()))
			return Arrays.deepEquals(this.tiles, ((Board)y).getTiles());
		return false;
	}
	
	// return a Collection of all neighboring board positions
	public Collection<Board> neighbors()
	{
		List<Board> boards = new LinkedList<Board>();
		int row = 0, col = 0;
		for(int i = 0; i < this.tiles.length; i++)
			for(int j = 0; j < this.tiles.length; j++)
				if(tiles[i][j] == 0){
					row = i; col = j; break;
				}
		
		//create new Boards
		if(col-1>=0){
			int[][] tiles1 = cloneArray(tiles);
			tiles1[row][col] = tiles[row][col-1];
			tiles1[row][col-1] = 0;
			boards.add(new Board(tiles1));
		}
		
		if(col+1<tiles.length){
			int[][] tiles2 = cloneArray(tiles);
			tiles2[row][col] = tiles[row][col+1];
			tiles2[row][col+1] = 0;
			boards.add(new Board(tiles2));
		}
		if(row+1<tiles.length){
			int[][] tiles3 = cloneArray(tiles);
			tiles3[row][col] = tiles[row+1][col];
			tiles3[row+1][col] = 0;
			boards.add(new Board(tiles3));
		}
		
		if(row-1>=0){
			int[][] tiles4 = cloneArray(tiles);
			tiles4[row][col] = tiles[row-1][col];
			tiles4[row-1][col] = 0;
			boards.add(new Board(tiles4));
		}
		
		return boards;
	}
	
	// return a string representation of the board
	public String toString()
	{
		String field = "";
		for(int i = 0; i < this.tiles.length; i++){
			for(int j = 0; j < this.tiles[i].length; j++)
				field += " " + this.tiles[i][j];
			field += "\n";
		}
		return field;
	}

	// is the initial board solvable?
	public boolean isSolvable()
	{
		int inversion = 0;
		double blankRow = 0;
		int[] flatArray = Arrays.stream(this.tiles).flatMapToInt(Arrays::stream).toArray();
		
		for(int i = 0; i < flatArray.length; i++){
			for(int j = i + 1; j < flatArray.length; j++)
				if(flatArray[i] > flatArray[j] && flatArray[j] != 0) inversion++;
			if(flatArray[i] == 0 && i != 0)
				blankRow = this.tiles.length - Math.ceil(i/(double)this.tiles.length) + 1;
			else if(flatArray[i] == 0 && i == 0)
				blankRow = this.tiles.length - Math.ceil(i/(double)this.tiles.length);
			
		}
		
		if(( (this.tiles.length % 2 != 0) && (inversion % 2 == 0) )  ||  
				( (this.tiles.length % 2 == 0) && ((blankRow % 2 != 0) == (inversion % 2 == 0)) ))
			return true;
		else
			return false;
	}
	
	/*
	 * Only for testing purposes
	 */
	public void setTiles(int[][] tiles){ this.tiles = tiles; }
	public int[][] getTiles(){ return this.tiles; }
	
	/*
	 * This method creates a complete new array this is to avoid that the initial state or the given
	 * state will be modified as we will ceate several boards of this state
	 */
	public static int[][] cloneArray(int[][] src) {
	    int length = src.length;
	    int[][] target = new int[length][src[0].length];
	    for (int i = 0; i < length; i++) {
	        System.arraycopy(src[i], 0, target[i], 0, src[i].length);
	    }
	    return target;
	}
}

