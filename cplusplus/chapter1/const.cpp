#include <iostream>

int main() {
    const int weightGoal = 70;
    std::cout << "goal: " << weightGoal << std::endl;
    weightGoal = 40; //error
    std::cout << "goal: " << weightGoal << std::endl;
    return 0;
}