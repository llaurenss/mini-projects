#include <iostream>

int main() {
    //MONDAY is zero
    //SATURDAY is 34
    //SUNDAY is 35 and not 6
    enum DAYS {MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY = 34, SUNDAY};

    DAYS day;
    day = MONDAY;
    std::cout << "day: " << day << std::endl;
    day = SUNDAY;
    std::cout << "day: " << day << std::endl;
    day = SATURDAY;
    std::cout << "day: " << day << std::endl;

    if (day == SATURDAY) {
        std::cout << "Hoera!" << std::endl;
    } else {
        std::cout << "Niet hoera!" << std::endl;
    }
    return 0;
}