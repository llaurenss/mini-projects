#include <iostream>

int main() {
    std::cout << "size of int: " << sizeof(int) << std::endl;
    std::cout << "size of short: " << sizeof(short) << std::endl;
    std::cout << "size of long: " << sizeof(long) << std::endl;
    std::cout << "size of float: " << sizeof(float) << std::endl;
    std::cout << "size of char: " << sizeof(char) << std::endl;
    std::cout << "size of double: " << sizeof(double) << std::endl;
    std::cout << "size of bool: " << sizeof(bool) << std::endl;
    return 0;
}