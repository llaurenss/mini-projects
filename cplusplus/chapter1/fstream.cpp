#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
    // write to a file
    ofstream myFile("input.txt", ios::app);
    f(myFile.is_open()) {
        myFile << "\nI am adding a line.\n";
        myFile << "I am adding another line.\n";
        myFile.close();
    } else {
        cout << "Not able to write to the file!" << endl;
    }

}