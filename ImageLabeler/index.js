class BoundingBox {
    constructor(x, y, width, height, name) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.name = this.setName(name);
    }

    setName(name) {
        if(name == '') {
            return "element";
        }
        return name;
    }

    equal(boundingBox) {
        if(boundingBox == undefined) { 
            return false; 
        }
        if(!boundingBox instanceof BoundingBox) {
            return false;
        }
        return this.x == boundingBox.x && this.y == boundingBox.y && this.width == boundingBox.width && this.height == boundingBox.height;
    }

    getCsvRepresentation() {
        return `${this.name}, ${this.x}, ${this.y}, ${this.width}, ${this.height}`
    }
}

class BoundingBoxList {
    constructor() {
        this.list = [];
    }

    boundingBoxExist(boundingBox) {
        // TODO convert to a lambda expression!
        for(let i = 0; i < this.list.length; i++) {
            if(boundingBox.equal(this.list[i])) {
                return true;
            }
        }
        return false;
    }

    add(boundingBox) {
        if (!this.boundingBoxExist(boundingBox)) {
            this.list.push(boundingBox);
        }
    }

    getBoundingBoxes() {
        return this.list;
    }
}

class BoundingBoxDraw {

    static drawBoundingBox(ctx, boundingBox) {
        ctx.beginPath();
        ctx.rect(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height);
        ctx.strokeStyle = 'black';
        ctx.lineWidth = 3;
        ctx.stroke();
    }
}

class ImageLabeler {
    constructor(canvas, boundingBoxList, realBoundingBoxList) {
        this.canvas = canvas;
        this.ctx = this.canvas.getContext('2d');
        this.boundingBoxList = boundingBoxList;
        this.realBoundingBoxList = realBoundingBoxList;

        this.mouseDown = false;
        this.canvasx = this.canvas.offsetLeft;
        this.canvasy = this.canvas.offsetTop;
        this.lastMouseX = 0;
        this.lastMouseY = 0;
        this.mouseX = 0;
        this.mouseY = 0;
    }

    drawPreviousBoxes() {
        this.boundingBoxList.getBoundingBoxes().forEach(element => {
            BoundingBoxDraw.drawBoundingBox(this.ctx, element);
        });
    }

    loadImage(imagePath) {
        this.image = new Image();
        this.image.onload = () => {
            let screenHeight = window.innerHeight;
            let screenWidth = window.innerWidth;
            this.canvas.height = screenHeight;
            this.canvas.width = screenWidth;
            this.drawImage();
        };
        this.image.src = imagePath;
    }

    getScaling() {
        let screenHeight = window.innerHeight;
        let screenWidth = window.innerWidth;
        let scalingFactorHeight = screenHeight/this.image.height;
        let scalingFactorWidth = screenWidth/this.image.width;
        if(scalingFactorHeight > scalingFactorWidth) return scalingFactorWidth;
        else return scalingFactorHeight;
    }

    drawImage() {
        if(this.image != undefined) {
            let scalingFactor = this.getScaling();
            let newImageWidth = scalingFactor *  this.image.width;
            let newImageHeight = scalingFactor *  this.image.height;
            this.ctx.drawImage(this.image, 0, 0, newImageWidth, newImageHeight);
        }
    }

    mouseDownAction = (e) => {
        this.lastMouseX = (e.clientX - this.canvasx);
        this.lastMouseY = (e.clientY - this.canvasy);
        this.mouseDown = true;
    }

    addBoundingBox = (e) => {
        let width = this.mouseX - this.lastMouseX;
        let height = this.mouseY - this.lastMouseY;
        let scalingFactor = this.getScaling();
        // convertion
        let x = parseInt(this.lastMouseX / scalingFactor);
        let y = parseInt(this.lastMouseY / scalingFactor);
        let realWidth = parseInt(width / scalingFactor);
        let realHeight = parseInt(height / scalingFactor);
        let elementName = elementNameTextfield.value;

        this.realBoundingBoxList.add(new BoundingBox(x, y, realWidth, realHeight, elementName));
        this.boundingBoxList.add(new BoundingBox(this.lastMouseX, this.lastMouseY, width, height, elementName));
    }

    mouseUpAction = (e) => {
        this.mouseDown = false;
    }

    mouseMoveAction = (e) => {
        if(this.mouseDown) { 
            this.mouseX = (e.clientX - this.canvasx);
            this.mouseY = (e.clientY - this.canvasy);
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.drawImage();
            this.drawPreviousBoxes();
            let width = this.mouseX - this.lastMouseX;
            let height = this.mouseY - this.lastMouseY;
            this.ctx.beginPath();
            this.ctx.rect(this.lastMouseX, this.lastMouseY, width, height);
            this.ctx.strokeStyle = 'black';
            this.ctx.lineWidth = 3;
            this.ctx.stroke();
        }
    }

    main(imagePath) {
        this.loadImage(imagePath);
    }
}

class AnnotationPrinter {
    constructor(boundingBoxList, imageName) {
        this.boundingBoxList = boundingBoxList;
        this.imageName = imageName;
    }

    printAnnotations = (e) => {
        let annotations = this.imageName + "\n";
        this.boundingBoxList.getBoundingBoxes().forEach(element => {
            annotations += element.getCsvRepresentation() + "\n";
        });
        annotationsTextArea.value = annotations;
    }
}

const path = 'test.jpg';
const canvas = document.getElementById('image');
const addBoxButton = document.getElementById('addElement')
const elementNameTextfield = document.getElementById('elementName')
const getAnnotationsButton = document.getElementById('getAnnotations');
const annotationsTextArea = document.getElementById('annotations');

let boundingBoxList = new BoundingBoxList();
let realBoundingBoxList = new BoundingBoxList();
let imageLabeler = new ImageLabeler(canvas, boundingBoxList, realBoundingBoxList);
let annotationPrinter = new AnnotationPrinter(realBoundingBoxList, path);

canvas.addEventListener('mousedown', imageLabeler.mouseDownAction);
canvas.addEventListener('mouseup', imageLabeler.mouseUpAction);
canvas.addEventListener('mousemove', imageLabeler.mouseMoveAction);
addBoxButton.addEventListener('click', imageLabeler.addBoundingBox);
getAnnotationsButton.addEventListener('click', annotationPrinter.printAnnotations);

imageLabeler.main(path);

