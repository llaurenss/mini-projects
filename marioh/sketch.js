var mario;
var levelBuilder;
var world;
var gameScreen;

function setup() {
    createCanvas(600, 300);
    gameScreen = new GameScreen();
    levelBuilder = new LevelBuilder();
    levelBuilder.setPlatforms();
    levelBuilder.setKeys();
    levelBuilder.setEnemies();
    levelBuilder.setMapActions();
    mario = new Player(30, 35, 25, 25, '#FF9900');

    world = new World();
    world.loadMap(levelBuilder);
    world.loadPlayer(mario);
}

function draw() {
    clear();
    background(51);
    world.drawWorld();
    world.update();
    world.draw();

}

function keyReleased() {
    mario.stop(key);
}

function LevelBuilder() {
    this.platforms = [];
    this.keys = [];
    this.enemies = [];
    this.worldWidth = 1600;
    this.worldHeight = 1200;
}

LevelBuilder.prototype.setPlatforms = function() {
    this.platforms = [
        new Platform(0, 0, 20, 300, 1),
        new Platform(0, 270, 400, 30, 1),
        new Platform(400, 280, 50, 20, 3),
        new Platform(375, 240, 25, 30, 1),
        new Platform(450, 270, 4000, 30, 1),
        new Platform(this.worldWidth - 25, 150, 20, 150, 6),

        new Platform(0, 100, 100, 20, 1),
        new Platform(100, 120, 20, 20, 1),
        new Platform(120, 140, 20, 20, 1),
        new Platform(140, 160, 20, 20, 1),
        new Platform(160, 180, 20, 20, 1),
        new Platform(180, 200, 20, 20, 1),
        new Platform(200, 220, 100, 20, 1),
        new Platform(900, 220, 100, 20, 1),

        new Platform(500, 230, 30, 20, 4),
        new Platform(40, 260, 15, 15, 5),
        new Platform(60, 260, 15, 15, 5),
    ];
}

LevelBuilder.prototype.setEnemies = function() {
    this.enemies = [
        new Enemy(1000, 0, 25, 25, '#966443'),
        new Enemy(1200, 0, 25, 25, '#966443'),
        new Enemy(390, 0, 25, 25, '#966443'),
    ]
}

LevelBuilder.prototype.setKeys = function() {
    this.keys = [
        { id: 1, solid: true, color: '#888' }, //wall
        { id: 2, solid: false, color: '#336699' }, //water
        { id: 3, solid: false, color: '#ff0000', die: true }, //lava
        { id: 4, solid: true, color: '#666666', gravity: -20 }, //jump      
        { id: 5, solid: false, color: '#FFD700', coin: true }, //coin
        { id: 6, solid: true, color: '#2d68b7', won: true } //won!
    ]
}

LevelBuilder.prototype.setMapActions = function() {
    for (var i = 0; i < this.platforms.length; i++) {
        for (var j = 0; j < this.keys.length; j++) {
            if (this.platforms[i].key == this.keys[j].id) {
                this.platforms[i].key = this.keys[j];
            }
        }
    }
}

LevelBuilder.prototype.removePlatform = function(index) {
    if (index > -1) {
        this.platforms.splice(index, 1);
    }
}

LevelBuilder.prototype.removeEnemy = function(index) {
    if (index > -1) {
        this.enemies.splice(index, 1);
    }
}

function Platform(x, y, width, height, key) {
    this.pos = createVector(x, y);
    this.width = width;
    this.height = height;
    this.key = key;
}

Platform.prototype.getBoundBox = function() {
    var offset = 5;
    return { x: this.pos.x, y: this.pos.y, w: this.width - offset, h: this.height };
}

Platform.prototype.drawPlatform = function() {
    fill(this.key.color);
    noStroke();
    if (this.key.id == 5) {
        ellipse(this.pos.x, this.pos.y, this.width);
    } else
        rect(this.pos.x, this.pos.y, this.width, this.height);
}

function Player(x, y, width, height, color) {
    this.pos = createVector(x, y);
    this.vel = createVector(0, 0);
    this.speed = createVector(6, -13);
    this.tmpJumpSpeed = 0;
    this.velLimit = createVector(2, 16);
    this.onFloor = false;
    this.width = width;
    this.height = height;
    this.color = color;
}

Player.prototype.stop = function(key) {
    if (key == '\'' || key == '%') {
        this.vel.x = 0;
    }
}

Player.prototype.draw = function() {
    fill(this.color);
    noStroke();
    rect(this.pos.x, this.pos.y, this.width, this.height);
}

function Enemy(x, y, width, height, color) {
    this.pos = createVector(x, y);
    this.width = width;
    this.height = height;
    this.vel = createVector(-1, 0);
    this.color = color;
    this.onFloor = false;
}

Enemy.prototype.draw = function() {
    fill(this.color);
    noStroke();
    rect(this.pos.x, this.pos.y, this.width, this.height);
}

function World() {
    this.levelBuilder;
    this.player;
    this.collectedCoinsLevel = 0;
    this.viewport = createVector(width, height);
    this.camera = createVector(0, 0);
}

World.prototype.overlap = function(a, b) {
    return a.x < b.x + b.w && a.x + a.w > b.x && a.y < b.y + b.h && a.y + a.h > b.y
}

World.prototype.loadPlayer = function(player) {
    this.player = player;
}

World.prototype.loadMap = function(levelBuilder) {
    this.levelBuilder = levelBuilder;
    this.setViewport();
}

World.prototype.setViewport = function() {
    this.offSetMaxX = this.levelBuilder.worldWidth - this.viewport.x;
    this.offSetMaxY = this.levelBuilder.worldHeight - this.viewport.y;
    this.offSetMinX = 0;
    this.offSetMinY = 0;
}

World.prototype.drawEnemies = function() {
    for (var i = 0; i < this.levelBuilder.enemies.length; i++) {
        this.levelBuilder.enemies[i].draw();
    }
}

World.prototype.drawWorld = function() {
    this.drawCoinsCollected();
    this.camera.x = this.player.pos.x - this.viewport.x / 2;
    this.camera.y = this.player.pos.y - this.viewport.y;
    if (this.camera.x > this.offSetMaxX) this.camera.x = this.offSetMaxX;
    else if (this.camera.x < this.offSetMinX) this.camera.x = this.offSetMinX;

    if (this.camera.y > this.offSetMaxY) this.camera.y = this.offSetMaxY;
    else if (this.camera.y < this.offSetMinY) this.camera.y = this.offSetMinY;
    translate(-this.camera.x, -this.camera.y);

    for (var i = 0; i < this.levelBuilder.platforms.length; i++) {
        this.levelBuilder.platforms[i].drawPlatform();
    }

    this.drawEnemies();
}

World.prototype.drawCoinsCollected = function() {
    textSize(12);
    fill('#FF');
    text('coins: ' + this.collectedCoinsLevel, width - 50, 15);
}

World.prototype.moveEnemy = function(enemy) {
    var vx = enemy.vel.x;
    var vy = enemy.vel.y;

    for (var i = 0; i < this.levelBuilder.platforms.length; i++) {
        var vx = enemy.vel.x;
        var c = { x: enemy.pos.x + vx, y: enemy.pos.y, w: enemy.width, h: enemy.height };
        if (this.overlap(c, this.levelBuilder.platforms[i].getBoundBox())) {
            var platform = this.levelBuilder.platforms[i];
            if (platform.key.solid) {
                if (vx < 0) enemy.vel.x = 1;
                else if (vx > 0) enemy.vel.x = -1;
            }
        }
    }
    enemy.pos.add(enemy.vel);

    for (var i = 0; i < this.levelBuilder.platforms.length; i++) {
        var c = { x: enemy.pos.x, y: enemy.pos.y + vy, w: enemy.width, h: enemy.height };
        if (this.overlap(c, this.levelBuilder.platforms[i].getBoundBox())) {
            var platform = this.levelBuilder.platforms[i];
            if (platform.key.solid) {
                if (vy < 0) vy = platform.pos.y + platform.height - enemy.pos.y
                if (vy > 0) vy = platform.pos.y - enemy.pos.y - enemy.height;
            }
        }
    }
    enemy.pos.y += vy;
}

World.prototype.checkEnemyPlayerCollision = function(enemy) {
    var e = { x: enemy.pos.x, y: enemy.pos.y, w: enemy.width, h: enemy.height };
    var c = { x: this.player.pos.x, y: this.player.pos.y, w: this.player.width, h: this.player.height };
    if (this.overlap(e, c) && this.player.onFloor) {
        gameScreen.gameOver();
        //console.log("ai ai ai");
    } else if (this.overlap(e, c) && !this.player.onFloor) {
        var index = this.levelBuilder.enemies.indexOf(enemy);
        this.levelBuilder.removeEnemy(index);
    }
}

World.prototype.move = function(vx, vy) {

    for (var i = 0; i < this.levelBuilder.platforms.length; i++) {
        var c = { x: this.player.pos.x + vx, y: this.player.pos.y, w: this.player.width, h: this.player.height };
        if (this.overlap(c, this.levelBuilder.platforms[i].getBoundBox())) {
            vx = this.collisionReaction(true, this.levelBuilder.platforms[i], vx, vy).vx;
        }
    }
    this.player.pos.x += vx;

    for (var i = 0; i < this.levelBuilder.platforms.length; i++) {
        var c = { x: this.player.pos.x, y: this.player.pos.y + vy, w: this.player.width, h: this.player.height };
        if (this.overlap(c, this.levelBuilder.platforms[i].getBoundBox())) {
            vy = this.collisionReaction(false, this.levelBuilder.platforms[i], vx, vy).vy;
        }
    }
    this.player.pos.y += vy;
}


World.prototype.collisionReaction = function(horizontal, platform, vx, vy) {
    if (horizontal) {
        if (platform.key.solid) {
            if (vx < 0) vx = platform.pos.x + platform.width - this.player.pos.x
            else if (vx > 0) vx = platform.pos.x - this.player.pos.x - this.player.width;
        }
    } else {
        if (platform.key.solid) {
            if (vy < 0) vy = platform.pos.y + platform.height - this.player.pos.y
            else if (vy > 0) vy = platform.pos.y - this.player.pos.y - this.player.height;
        }
        if (platform.key.gravity) {
            this.player.tmpJumpSpeed = platform.key.gravity;
        } else {
            this.player.tmpJumpSpeed = 0;
        }

        if (platform.key.die) {
            gameScreen.gameOver();
        }
        if (platform.key.coin) {
            this.collectedCoinsLevel += 1;
            var index = this.levelBuilder.platforms.indexOf(platform);
            this.levelBuilder.removePlatform(index);
        }
    }
    if (platform.key.won) {
        gameScreen.gameWon();
    }
    return { vx: vx, vy: vy };
}

World.prototype.update = function() {

    if (keyIsDown(LEFT_ARROW)) {
        if (this.player.vel.x > -this.player.velLimit.x)
            this.player.vel.x -= this.player.speed.x;
    }

    if (keyIsDown(RIGHT_ARROW)) {
        if (this.player.vel.x < this.player.velLimit.x)
            this.player.vel.x += this.player.speed.x;
    }
    this.player.vel.y += 1;
    for (var i = 0; i < this.levelBuilder.enemies.length; i++) {
        this.levelBuilder.enemies[i].vel.y += 1;
    }

    var expectedY = this.player.pos.y + this.player.vel.y
    this.move(this.player.vel.x, this.player.vel.y);
    this.player.onFloor = (expectedY > this.player.pos.y);
    if (expectedY != this.player.pos.y) this.player.vel.y = 0;

    for (var i = 0; i < this.levelBuilder.enemies.length; i++) {
        var enemy = this.levelBuilder.enemies[i];
        var expectedEnemyY = enemy.pos.y + enemy.vel.y;
        this.moveEnemy(enemy);
        enemy.onFloor = (expectedEnemyY > enemy.pos.y);
        if (expectedEnemyY != enemy.pos.y) this.levelBuilder.enemies[i].vel.y = 0;
        this.checkEnemyPlayerCollision(enemy);
    }


    if (this.player.onFloor && keyIsDown(UP_ARROW)) {
        if (this.player.tmpJumpSpeed == 0) {
            this.player.vel.y = this.player.speed.y;
        } else {
            this.player.vel.y = this.player.tmpJumpSpeed;
            this.player.tmpJumpSpeed = 0;
        }

    }
}

World.prototype.draw = function() {
    this.player.draw();
}

function GameScreen() {
    this.keys = [];
    this.setKeys();
}

GameScreen.prototype.setKeys = function() {
    this.keys = [
        { title: "Game Over!", backgroundColor: '#1c1c1c', textColor: '#ab1323', textSize: 32 },
        { title: "You won!!!", backgroundColor: '#1c1c1c', textColor: '#ab1323', textSize: 32 }
    ]
}

GameScreen.prototype.gameOver = function() {
    translate(world.camera.x, world.camera.y);
    fill(this.keys[0].backgroundColor);
    rect(0, 0, width, height);
    textSize(32);
    fill(this.keys[0].textColor);
    text(this.keys[0].title, width / 3, height / 2);
    frameRate(0);
}

GameScreen.prototype.gameWon = function() {
    translate(world.camera.x, world.camera.y);
    fill(this.keys[1].backgroundColor);
    rect(0, 0, width, height);
    textSize(32);
    fill(this.keys[1].textColor);
    text(this.keys[1].title, width / 3, height / 2);
    frameRate(0);
}