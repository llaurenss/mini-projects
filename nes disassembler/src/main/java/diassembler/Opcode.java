package diassembler;

/**
 * resc:
 *  http://www.thealmightyguru.com/Games/Hacking/Wiki/index.php/6502_Opcodes
 * 
 * 
 * 
 * 
 * examples:
 *      0001:A9 20     LDA #$20         ; Loads #$20 into the accumulator.
 *      0009:4C 05 00  JMP $0005        ; Jump the program counter to $0005.
 *      0001:18        CLC              ; Clears the carry flag.
 */
public final class Opcode {
    private final String hexString;
    private final String mnemonic;
    private final AddressingMode mode;
    private final int bytes;
    private final int cycles;
    private boolean boundaryCrossed;
    
    public Opcode(String hexString, String mnemonic, AddressingMode mode, int bytes, int cycles){
        this.hexString = hexString;
        this.mnemonic = mnemonic;
        this.mode = mode;
        this.bytes = bytes;
        this.cycles = cycles;
        boundaryCrossed = false;
    }
    
    public Opcode(String hexString, String mnemonic, AddressingMode mode, int bytes, int cycles, boolean boundaryCrossed){
        this(hexString, mnemonic, mode, bytes, cycles);
        this.boundaryCrossed = boundaryCrossed;
    }
    
    public void setBoundaryCrossed(){
        this.boundaryCrossed = true;
    }
    
    public String getMnemonic(){
        return this.mnemonic;
    }
    
    public String getHexString(){
        return "0x" + this.hexString;
    }
    
    public int getByte() {
        int v = Integer.parseInt(this.getHexString().substring(2), 16);
        return (byte) v;
    }
    
    public int getCycles() {
        return this.cycles;
    }
    
    public int getBytes() {
        return this.bytes;
    }
    
    @Override public String toString() {
        return mnemonic; 
    }
    @Override public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Opcode)) {
            return false;
        }
        Opcode opcode = (Opcode)o;
        return opcode.mnemonic.equals(mnemonic) && opcode.bytes == bytes && opcode.cycles == cycles;
    }
    
    @Override public int hashCode() {
        return mnemonic.length() + bytes + cycles;
    }
}
