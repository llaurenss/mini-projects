package diassembler;

import disasembler.exceptions.OpcodeNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Diassembler {
    private final byte[] fileContents;
    
    public Diassembler(byte[] fileContents){
        this.fileContents = fileContents;
    }
    
    private Opcode getOpcode(byte b) {
        FetchOpcode fetchOpcode = new FetchOpcode();
        try {
            Opcode opcode = fetchOpcode.fetchOpcode(b);
            return opcode;
        }catch (OpcodeNotFoundException one){
            return new Opcode("NA", "NA", AddressingMode.IMPLIED, 1, 1);
        }
    }
    public List<String> getAssemblyCode()
    {
        List<String> assemblyCode = new ArrayList<>();
        for(int i = 0; i < this.fileContents.length; i++) {
            Opcode opcode = getOpcode(this.fileContents[i]);
            String instruction = buildOutput(opcode, i);
            assemblyCode.add(instruction);
            i = i + (opcode.getBytes() - 1);
        }
        return assemblyCode;
    }
    
    private String buildHexInstruction(Opcode opcode, int index) {
        String instruction = ByteUtil.getHexStringFromByte(this.fileContents[index]);
        int length = opcode.getBytes();
        
        if (length == 2) {
            instruction += " $" + ByteUtil.getHexStringFromByte(this.fileContents[index + 1]);
        }
        
        if (length == 3) {
            instruction += " $" + ByteUtil.getHexStringFromByte(this.fileContents[index + 2]);
            instruction += " " + ByteUtil.getHexStringFromByte(this.fileContents[index + 1]);
        }
        return instruction;
    }
    
    private String buildInstruction(Opcode opcode, int index) {
        String instruction = opcode.getMnemonic();
        int length = opcode.getBytes();
        
        if (length == 2) {
            instruction += " $" + ByteUtil.getHexStringFromByte(this.fileContents[index + 1]);
        }
        
        if (length == 3) {
            instruction += " $" + ByteUtil.getHexStringFromByte(this.fileContents[index + 2]);
            instruction += " " + ByteUtil.getHexStringFromByte(this.fileContents[index + 1]);
        }
        return instruction;
    }
    
    private String buildOutput(Opcode opcode, int index) {
        String hexInstruction = buildHexInstruction(opcode, index);
        String instruction = buildInstruction(opcode, index);
        return String.format("s%-20s%s", hexInstruction, instruction);
    }
    
}
