
package diassembler;
public class ByteUtil {
    static String getHexStringFromByte(byte b) {
        return String.format("%02x", b);
    }
}