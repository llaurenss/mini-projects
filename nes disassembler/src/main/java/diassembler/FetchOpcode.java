package diassembler;

import disasembler.exceptions.OpcodeNotFoundException;
import java.util.ArrayList;
import java.util.List;

public final class FetchOpcode {

    private final List<Opcode> opcodes;

    public FetchOpcode() {
        this.opcodes = new ArrayList<>();
        this.setOpcodes();
    }

    public void setOpcodes() {
        this.opcodes.add(new Opcode("29", "AND", AddressingMode.IMMEDIATE, 2, 2));
        this.opcodes.add(new Opcode("25", "AND", AddressingMode.ZERO_PAGE, 2, 3));
        this.opcodes.add(new Opcode("35", "AND", AddressingMode.ZERO_PAGE_X, 2, 4));
        this.opcodes.add(new Opcode("2D", "AND", AddressingMode.ABSOLUTE, 3, 4));
        this.opcodes.add(new Opcode("3D", "AND", AddressingMode.ABSOLUTE_X, 3, 4, true));
        this.opcodes.add(new Opcode("39", "AND", AddressingMode.ABSOLUTE_Y, 3, 4, true));
        this.opcodes.add(new Opcode("21", "AND", AddressingMode.INDIRECT_X, 2, 6));
        this.opcodes.add(new Opcode("31", "AND", AddressingMode.INDIRECT_Y, 2, 5, true));
        this.opcodes.add(new Opcode("00", "BRK", AddressingMode.IMMEDIATE, 1, 7));
        this.opcodes.add(new Opcode("18", "CLC", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("D8", "CLD", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("58", "CLI", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("B8", "CLV", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("49", "EOR", AddressingMode.IMMEDIATE, 2, 2));
        this.opcodes.add(new Opcode("45", "EOR", AddressingMode.ZERO_PAGE, 2, 3));
        this.opcodes.add(new Opcode("55", "EOR", AddressingMode.ZERO_PAGE_X, 2, 4));
        this.opcodes.add(new Opcode("4D", "EOR", AddressingMode.ABSOLUTE, 3, 4));
        this.opcodes.add(new Opcode("5D", "EOR", AddressingMode.ABSOLUTE_X, 3, 4, true));
        this.opcodes.add(new Opcode("59", "EOR", AddressingMode.ABSOLUTE_Y, 3, 4, true));
        this.opcodes.add(new Opcode("41", "EOR", AddressingMode.INDIRECT_X, 2, 6));
        this.opcodes.add(new Opcode("51", "EOR", AddressingMode.INDIRECT_Y, 2, 5, true));
        this.opcodes.add(new Opcode("A9", "LDA", AddressingMode.IMMEDIATE, 2, 2));
        this.opcodes.add(new Opcode("A5", "LDA", AddressingMode.ZERO_PAGE, 2, 3));
        this.opcodes.add(new Opcode("B5", "LDA", AddressingMode.ZERO_PAGE_X, 2, 4));
        this.opcodes.add(new Opcode("AD", "LDA", AddressingMode.ABSOLUTE, 3, 4));
        this.opcodes.add(new Opcode("BD", "LDA", AddressingMode.ABSOLUTE_X, 3, 4, true));
        this.opcodes.add(new Opcode("B9", "LDA", AddressingMode.ABSOLUTE_Y, 3, 4, true));
        this.opcodes.add(new Opcode("A1", "LDA", AddressingMode.INDIRECT_X, 2, 6));
        this.opcodes.add(new Opcode("B1", "LDA", AddressingMode.INDIRECT_Y, 2, 5, true));
        this.opcodes.add(new Opcode("A6", "LDX", AddressingMode.ZERO_PAGE, 2, 2));
        this.opcodes.add(new Opcode("B6", "LDX", AddressingMode.ZERO_PAGE_Y, 2, 3));
        this.opcodes.add(new Opcode("AE", "LDX", AddressingMode.ABSOLUTE, 2, 4));
        this.opcodes.add(new Opcode("BE", "LDX", AddressingMode.ABSOLUTE_Y, 3, 4));
        this.opcodes.add(new Opcode("A2", "LDX", AddressingMode.IMMEDIATE, 3, 4, true));
        this.opcodes.add(new Opcode("A0", "LDY", AddressingMode.IMMEDIATE, 2, 2));
        this.opcodes.add(new Opcode("A4", "LDY", AddressingMode.ZERO_PAGE, 2, 3));
        this.opcodes.add(new Opcode("B4", "LDY", AddressingMode.ZERO_PAGE_X, 2, 4));
        this.opcodes.add(new Opcode("AC", "LDY", AddressingMode.ABSOLUTE, 3, 4));
        this.opcodes.add(new Opcode("BC", "LDY", AddressingMode.ABSOLUTE_X, 3, 4, true));
        this.opcodes.add(new Opcode("EA", "NOP", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("09", "ORA", AddressingMode.IMMEDIATE, 2, 2));
        this.opcodes.add(new Opcode("05", "ORA", AddressingMode.ZERO_PAGE, 2, 3));
        this.opcodes.add(new Opcode("15", "ORA", AddressingMode.ZERO_PAGE_X, 2, 4));
        this.opcodes.add(new Opcode("0D", "ORA", AddressingMode.ABSOLUTE, 3, 4));
        this.opcodes.add(new Opcode("1D", "ORA", AddressingMode.ABSOLUTE_X, 3, 4, true));
        this.opcodes.add(new Opcode("19", "ORA", AddressingMode.ABSOLUTE_Y, 3, 4, true));
        this.opcodes.add(new Opcode("01", "ORA", AddressingMode.INDIRECT_X, 2, 6));
        this.opcodes.add(new Opcode("11", "ORA", AddressingMode.INDIRECT_Y, 2, 5, true));
        this.opcodes.add(new Opcode("38", "SEC", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("F8", "SED", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("78", "SEI", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("85", "STA", AddressingMode.ZERO_PAGE, 2, 3));
        this.opcodes.add(new Opcode("95", "STA", AddressingMode.ZERO_PAGE_X, 2, 4));
        this.opcodes.add(new Opcode("8D", "STA", AddressingMode.ABSOLUTE, 3, 4));
        this.opcodes.add(new Opcode("9D", "STA", AddressingMode.ABSOLUTE_X, 3, 4, true));
        this.opcodes.add(new Opcode("99", "STA", AddressingMode.ABSOLUTE_Y, 3, 4, true));
        this.opcodes.add(new Opcode("81", "STA", AddressingMode.INDIRECT_X, 2, 6));
        this.opcodes.add(new Opcode("91", "STA", AddressingMode.INDIRECT_Y, 2, 5, true));
        this.opcodes.add(new Opcode("86", "STX", AddressingMode.ZERO_PAGE, 2, 3));
        this.opcodes.add(new Opcode("96", "STX", AddressingMode.ZERO_PAGE_Y, 2, 4));
        this.opcodes.add(new Opcode("8E", "STX", AddressingMode.ABSOLUTE, 3, 4));
        this.opcodes.add(new Opcode("84", "STY", AddressingMode.ZERO_PAGE, 2, 3));
        this.opcodes.add(new Opcode("94", "STY", AddressingMode.ZERO_PAGE_X, 2, 4));
        this.opcodes.add(new Opcode("8C", "STY", AddressingMode.ABSOLUTE, 3, 4));
        this.opcodes.add(new Opcode("AA", "TAX", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("A8", "TAY", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("BA", "TSX", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("8A", "TXA", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("9A", "TXS", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("98", "TYA", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("69", "ADC", AddressingMode.IMMEDIATE, 2, 2));
        this.opcodes.add(new Opcode("65", "ADC", AddressingMode.ZERO_PAGE, 2, 3));
        this.opcodes.add(new Opcode("75", "ADC", AddressingMode.ZERO_PAGE_X, 2, 4));
        this.opcodes.add(new Opcode("6D", "ADC", AddressingMode.ABSOLUTE, 3, 4));
        this.opcodes.add(new Opcode("7D", "ADC", AddressingMode.ABSOLUTE_X, 3, 4, true));
        this.opcodes.add(new Opcode("79", "ADC", AddressingMode.ABSOLUTE_Y, 3, 4, true));
        this.opcodes.add(new Opcode("61", "ADC", AddressingMode.INDIRECT_X, 2, 6));
        this.opcodes.add(new Opcode("71", "ADC", AddressingMode.INDIRECT_Y, 2, 5, true));
        this.opcodes.add(new Opcode("0A", "ASL", AddressingMode.ACCUMULATOR, 1, 2));
        this.opcodes.add(new Opcode("06", "ASL", AddressingMode.ZERO_PAGE, 2, 5));
        this.opcodes.add(new Opcode("16", "ASL", AddressingMode.ZERO_PAGE_X, 2, 6));
        this.opcodes.add(new Opcode("0E", "ASL", AddressingMode.ABSOLUTE, 3, 6));
        this.opcodes.add(new Opcode("1E", "ASL", AddressingMode.ABSOLUTE_X, 3, 7));
        this.opcodes.add(new Opcode("90", "BCC", AddressingMode.RELATIVE, 2, 2, true));
        this.opcodes.add(new Opcode("B0", "BCS", AddressingMode.RELATIVE, 2, 2, true));
        this.opcodes.add(new Opcode("F0", "BEQ", AddressingMode.RELATIVE, 2, 2, true));
        this.opcodes.add(new Opcode("24", "BIT", AddressingMode.ZERO_PAGE, 2, 3));
        this.opcodes.add(new Opcode("2C", "BIT", AddressingMode.ABSOLUTE, 3, 4));
        this.opcodes.add(new Opcode("30", "BMI", AddressingMode.RELATIVE, 2, 2, true));
        this.opcodes.add(new Opcode("D0", "BNE", AddressingMode.RELATIVE, 2, 2, true));
        this.opcodes.add(new Opcode("10", "BPL", AddressingMode.RELATIVE, 2, 2, true));
        this.opcodes.add(new Opcode("50", "BVC", AddressingMode.RELATIVE, 2, 2, true));
        this.opcodes.add(new Opcode("70", "BVS", AddressingMode.RELATIVE, 2, 2, true));
        this.opcodes.add(new Opcode("C9", "CMP", AddressingMode.IMMEDIATE, 2, 2));
        this.opcodes.add(new Opcode("C5", "CMP", AddressingMode.ZERO_PAGE, 2, 3));
        this.opcodes.add(new Opcode("D5", "CMP", AddressingMode.ZERO_PAGE_X, 2, 4));
        this.opcodes.add(new Opcode("CD", "CMP", AddressingMode.ABSOLUTE, 3, 4));
        this.opcodes.add(new Opcode("DD", "CMP", AddressingMode.ABSOLUTE_X, 3, 4, true));
        this.opcodes.add(new Opcode("D9", "CMP", AddressingMode.ABSOLUTE_Y, 3, 4, true));
        this.opcodes.add(new Opcode("C1", "CMP", AddressingMode.INDIRECT_X, 2, 6));
        this.opcodes.add(new Opcode("D1", "CMP", AddressingMode.INDIRECT_Y, 2, 5, true));
        this.opcodes.add(new Opcode("E0", "CPX", AddressingMode.IMMEDIATE, 2, 2));
        this.opcodes.add(new Opcode("E4", "CPX", AddressingMode.ZERO_PAGE, 2, 3));
        this.opcodes.add(new Opcode("EC", "CPX", AddressingMode.ABSOLUTE, 3, 4));
        this.opcodes.add(new Opcode("C0", "CPY", AddressingMode.IMMEDIATE, 2, 2));
        this.opcodes.add(new Opcode("C4", "CPY", AddressingMode.ZERO_PAGE, 2, 3));
        this.opcodes.add(new Opcode("CC", "CPY", AddressingMode.ABSOLUTE, 3, 4));
        this.opcodes.add(new Opcode("C6", "DEC", AddressingMode.ZERO_PAGE, 2, 5));
        this.opcodes.add(new Opcode("D6", "DEC", AddressingMode.ZERO_PAGE_X, 2, 6));
        this.opcodes.add(new Opcode("CE", "DEC", AddressingMode.ABSOLUTE, 3, 6));
        this.opcodes.add(new Opcode("DE", "DEC", AddressingMode.ABSOLUTE_X, 3, 7));
        this.opcodes.add(new Opcode("CA", "DEX", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("88", "DEY", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("E6", "INC", AddressingMode.ZERO_PAGE, 2, 5));
        this.opcodes.add(new Opcode("F6", "INC", AddressingMode.ZERO_PAGE_X, 2, 6));
        this.opcodes.add(new Opcode("EE", "INC", AddressingMode.ABSOLUTE, 3, 6));
        this.opcodes.add(new Opcode("FE", "INC", AddressingMode.ABSOLUTE_X, 3, 7));
        this.opcodes.add(new Opcode("E8", "INX", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("C8", "INY", AddressingMode.IMPLIED, 1, 2));
        this.opcodes.add(new Opcode("6C", "JMP", AddressingMode.INDIRECT, 3, 5));
        this.opcodes.add(new Opcode("4C", "JMP", AddressingMode.ABSOLUTE, 3, 3));
        this.opcodes.add(new Opcode("20", "JSR", AddressingMode.ABSOLUTE, 3, 6));
        this.opcodes.add(new Opcode("4A", "LSR", AddressingMode.ACCUMULATOR, 1, 2));
        this.opcodes.add(new Opcode("46", "LSR", AddressingMode.ZERO_PAGE, 2, 5));
        this.opcodes.add(new Opcode("56", "LSR", AddressingMode.ZERO_PAGE_X, 2, 6));
        this.opcodes.add(new Opcode("4E", "LSR", AddressingMode.ABSOLUTE, 3, 6));
        this.opcodes.add(new Opcode("5E", "LSR", AddressingMode.ABSOLUTE_X, 3, 7));
        this.opcodes.add(new Opcode("48", "PHA", AddressingMode.IMPLIED, 1, 3));
        this.opcodes.add(new Opcode("08", "PHP", AddressingMode.IMPLIED, 1, 3));
        this.opcodes.add(new Opcode("68", "PLA", AddressingMode.IMPLIED, 1, 4));
        this.opcodes.add(new Opcode("28", "PLP", AddressingMode.IMPLIED, 1, 4));
        this.opcodes.add(new Opcode("2A", "ROL", AddressingMode.ACCUMULATOR, 1, 2));
        this.opcodes.add(new Opcode("26", "ROL", AddressingMode.ZERO_PAGE, 2, 5));
        this.opcodes.add(new Opcode("36", "ROL", AddressingMode.ZERO_PAGE_X, 2, 6));
        this.opcodes.add(new Opcode("2E", "ROL", AddressingMode.ABSOLUTE, 3, 6));
        this.opcodes.add(new Opcode("3E", "ROL", AddressingMode.ABSOLUTE_X, 3, 6));
        this.opcodes.add(new Opcode("6A", "ROR", AddressingMode.ACCUMULATOR, 1, 2));
        this.opcodes.add(new Opcode("66", "ROR", AddressingMode.ZERO_PAGE, 2, 5));
        this.opcodes.add(new Opcode("76", "ROR", AddressingMode.ZERO_PAGE_X, 2, 6));
        this.opcodes.add(new Opcode("6E", "ROR", AddressingMode.ABSOLUTE, 3, 6));
        this.opcodes.add(new Opcode("7E", "ROR", AddressingMode.ABSOLUTE_X, 3, 7));
        this.opcodes.add(new Opcode("40", "RTI", AddressingMode.IMPLIED, 1, 6));
        this.opcodes.add(new Opcode("60", "RTS", AddressingMode.IMPLIED, 1, 6));
        this.opcodes.add(new Opcode("E9", "SBC", AddressingMode.IMMEDIATE, 2, 2));
        this.opcodes.add(new Opcode("E5", "SBC", AddressingMode.ZERO_PAGE, 2, 3));
        this.opcodes.add(new Opcode("F5", "SBC", AddressingMode.ZERO_PAGE_X, 2, 4));
        this.opcodes.add(new Opcode("ED", "SBC", AddressingMode.ABSOLUTE, 3, 4));
        this.opcodes.add(new Opcode("FD", "SBC", AddressingMode.ABSOLUTE_X, 3, 4, true));
        this.opcodes.add(new Opcode("F9", "SBC", AddressingMode.ABSOLUTE_Y, 3, 4, true));
        this.opcodes.add(new Opcode("E1", "SBC", AddressingMode.INDIRECT_X, 2, 6));
        this.opcodes.add(new Opcode("F1", "SBC", AddressingMode.INDIRECT_Y, 2, 5, true));
    }

    public Opcode fetchOpcode(byte b) throws OpcodeNotFoundException {
        for (Opcode opcode : this.opcodes) {
            if (opcode.getByte() == b) {
                return opcode;
            }
        }
        throw new OpcodeNotFoundException("Opcode was not found!");
    }
}
