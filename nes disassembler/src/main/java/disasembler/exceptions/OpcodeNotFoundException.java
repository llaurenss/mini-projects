package disasembler.exceptions;

public class OpcodeNotFoundException extends Exception {

    private static final long serialVersionUID = 7718828512143293558L;

    public OpcodeNotFoundException() {
        super();
    }

    public OpcodeNotFoundException(String message) {
        super(message);
    }
    
    public OpcodeNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
