package fileIO;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileIO {
    private static final byte[] NO_FILE = {};
    
    public byte[] readFile(String nesFile) {
        try {
            Path path = Paths.get(nesFile);
            byte[] fileContents =  Files.readAllBytes(path);
            return fileContents;
        } catch (IOException ex) {
            Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return NO_FILE;
    }
}
