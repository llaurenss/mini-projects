#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define ROWS 3
#define BOARD_SIZE ROWS * ROWS

bool enter_entry(char *board, char character, int x, int y);
void print_board(const char *board);
void *init_board();
bool is_solved(char *board, char item);
bool is_draw(char *board);
bool is_equal(char *board, int indexA, int indexB, int indexC, char item);
void player_selection(char *board, char item);
void game_loop(char *board);

int main()
{
    char *board = init_board();
    game_loop(board);
    return 0;
}

void *init_board()
{
    char *board = malloc(sizeof(char) * BOARD_SIZE);
    if(!board) { 
        printf("Memory allocation failed");
        return NULL;
    } else {     
        for(int i = 0; i < BOARD_SIZE; i++) {
            board[i] = ' ';
        }
    }
    return board;
}

void print_board(const char *board)
{
    for(int i = 1; i < BOARD_SIZE+1; i++)
    {
        printf("| %c ", board[i-1]);
        if(i % 3 == 0)
        {
            printf("|\n");        
        }
    }
    printf("\n");
}

bool is_solved(char *board, char item)
{
    if(is_equal(board, 0, 1, 2, item)) { return true; }
    if(is_equal(board, 3, 4, 5, item)) { return true; }
    if(is_equal(board, 6, 7, 8, item)) { return true; }

    if(is_equal(board, 0, 3, 6, item)) { return true; }
    if(is_equal(board, 1, 4, 7, item)) { return true; }
    if(is_equal(board, 2, 5, 8, item)) { return true; }

    if(is_equal(board, 0, 4, 8, item)) { return true; }
    if(is_equal(board, 2, 4, 6, item)) { return true; }
    return false;
}

bool is_draw(char *board)
{
    for(int i = 0; i < BOARD_SIZE; i++)
    {
        if(board[i] == ' ')
        {
            return false;
        }
    }
    return true;
}

bool is_equal(char *board, int indexA, int indexB, int indexC, char item)
{
    if(board[indexA] == item && board[indexB] == item && board[indexC] == item)
    {
        return true;
    }
    return false;
}

bool enter_entry(char *board, char character, int x, int y)
{
    int index = x + ROWS * y;

    if(board[index] != ' ')
    {
        return false;
    }

    board[index] = character;
    return true;
}

void player_selection(char *board, char item)
{
    int x, y;
    printf("enter coords (x, y): \n");

    while(1)
    {
        scanf(" %d %d", &x, &y);
        if(scanf(" %d %d", &x, &y) != 2) 
        {
            printf("Could not read the input");
        } 

        bool succes = enter_entry(board, item, x-1, y-1);
        if(succes)
        {
            break;
        }
        printf("This coord is already used or not valid enter new ones:\n");
    }
    print_board(board);
}

void game_loop(char *board)
{
    const char playerOneChar = 'o';
    const char playerTwoChar = 'x';

    printf("Welcome to tic tac toe!\n");
    printf("Press enter to continue\n");
    char enter = 0;
    while (enter != '\r' && enter != '\n') { enter = getchar(); }

    printf("Let's start the game!\n");
    print_board(board);

    while(1)
    {
        printf("Player one: \n");
        player_selection(board, playerOneChar);
        if(is_solved(board, playerOneChar))
        {
            printf("PLayer one won!\n");
            break;
        }

        if(is_draw(board))
        {
            printf("No winners!\n");
            break;
        }

        printf("Player two: \n");
        player_selection(board, playerTwoChar);
        if(is_solved(board, playerTwoChar))
        {
            printf("PLayer two won!");
            break;
        }
        if(is_draw(board))
        {
            printf("No winners!\n");
            break;
        }
    }
}