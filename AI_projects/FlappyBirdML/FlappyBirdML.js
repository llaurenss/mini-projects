let bird;
let pipes = [];
let classifier;

function preload() {
	classifier = ml5.soundClassifier('SpeechCommands18w');
}

function setup() {
	createCanvas(400, 600);
	classifier.classify(gotResult);
    bird = new Bird();
    pipes.push(new Pipe());
}	

function draw() {
    clear();
    background(51);
    for (let i = pipes.length-1; i >= 0; i--) {
    	pipes[i].draw();
    	pipes[i].update();
		if (pipes[i].hits(bird)) console.log("HIT");
		if (pipes[i].offscreen()) pipes.splice(i, 1);
  	}
	bird.update();
    bird.draw();

   	if (frameCount % 100 == 0) pipes.push(new Pipe());
}

function keyPressed() {
	if (key == ' ') {
		bird.up();
	}
}

function gotResult(error, result) {
	if (error) {
		console.log(error);
		return;
	}

	console.log(result[0]);
	if (result[0].label == 'up' && result[0].confidence > 0.90) {
		bird.up();
	}
}

function Bird() {
	this.pos = createVector(30, height/2);
	this.width = 32;
	this.height = 32;
	this.gravity = 0.6;
	this.lift = -15;
	this.vel = 0;


	this.draw = function() {
		fill(255);
		ellipse(this.pos.x, this.pos.y, this.width, this.height);
	}

	this.update = function() {
		this.vel += this.gravity;
		this.vel.y *= 0.9;
		this.pos.y += this.vel;

		if (this.pos.y > height) {
			this.pos.y = height;
			this.vel = 0;
		}

		if (this.y < 0) {
			this.pos.y = 0;
			this.vel = 0;
		}
	}

	this.up = function() {
		this.vel += this.lift;
	}
}

function Pipe() {
	this.top = random(height/2);
	this.bottom = random(height/2);
	this.x = width;
	this.w = 20;
	this.speed = 2;
	this.highlight = false;

	this.offscreen = function() {
		return this.x  < -this.w;
	}

	this.draw = function() {
		fill(255);
		if (this.highlight) fill(255, 0, 0);
		rect(this.x, 0, this.w, this.top);
		rect(this.x, height - this.bottom, this.w, this.bottom);
	}

	this.update = function() {
		this.x -= this.speed;
	}

	this.hits = function(bird) {
		if (bird.pos.y < this.top || bird.pos.y > height - this.bottom) {
      		if (bird.pos.x > this.x && bird.pos.x < this.x + this.w) {
        		this.highlight = true;
        		return true;
      		}
    	}
    	this.highlight = false;
    	return false;
	}
}