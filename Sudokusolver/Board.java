package sudokuBacktracking;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Board {
	
	private int[][] board;
	private int size = 9;
	
	public Board(int[][] board) {
		this.board = board;
	}

	public boolean isSolved() {
		return checkSums() && isUnique();
	}
	
	private boolean checkSums() {
		int som = 0;
		int totalSom = 0;
		for(int i = 0; i < board.length; i++){
			for(int j = 0; j < board[i].length; j++){
				som += board[i][j];
			}
			if(som != 45) return false;
			totalSom += som;
			som = 0;
		}
		return totalSom == 405;
	}
	
	private boolean isUnique() {
		for(int i = 0; i < board.length; i++){
			int tmp[] = board[i];
			if (duplicates(tmp)) return false;
		}
		return true;
	}
	
	private boolean duplicates(int[] input) {
		List<int[]> inputList = Arrays.asList(input);
        Set<int[]> inputSet = new HashSet<int[]>(inputList);
        return inputSet.size()< inputList.size();
	}
	
	public String toString() { 
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < this.size; i++){
			for(int j = 0; j < this.size; j++){
				sb.append(this.board[i][j]+ " ");
			}
			sb.append(System.getProperty("line.separator"));
		}
		return sb.toString();
	}
}
