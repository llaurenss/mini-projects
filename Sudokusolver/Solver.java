package sudokuBacktracking;

public class Solver {
	
	private int[][] board;
	
	public Solver(int[][] board) {
		this.board = board;
	}
	
	public int[][] getSolution(){
		return this.board;
	}

	public boolean solve() {
		int row;
		int col;
		int[] blank = findNextBlank();
		row = blank[0];
		col = blank[1];
		if (row == -1) return true;
		for (int i = 1; i <= 9; i++) {
			if (isSafe(row, col, i)) {
				this.board[row][col] = i;
				if (solve()) return true;
				this.board[row][col] = 0;
			}
		}
		return false;
	}
	
	/*
	 * If we can't find a blank position => grid is full => return (-1,-1) else return cell position
	 */
	private int[] findNextBlank() {
		int[] cell = new int[2];
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (this.board[i][j] == 0) {
					cell[0] = i;
					cell[1] = j;
					return cell;
				}
			}
		}
		cell[0] = -1;
		cell[1] = -1;
		return cell;
	}
	
	private boolean isSafe(int row, int col, int n) {
		return !inRow(row, n) && !inColumn(col, n) && !inBox(row - row % 3, col - col % 3, n);
	}
	
	private boolean inRow(int row, int n) {
		for (int i = 0; i < 9; i++) {
			if (this.board[row][i] == n) {
				return true;
			}
		}
		return false;
	}
	
	private boolean inColumn(int col, int n) {
		for (int i = 0; i < 9; i++) {
			if (this.board[i][col] == n) {
				return true;
			}
		}
		return false;
	}
	
	private boolean inBox(int boxStartRow, int boxStartCol, int n) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (this.board[i + boxStartRow][j + boxStartCol] == n) {
					return true;
				}
			}
		}
		return false;
	}
}
